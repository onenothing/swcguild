function rollDice(){
	var initialWagerValue = (document.getElementById("inputWagerField").value);
	var wagerValue = Number(initialWagerValue);

	var die1 = 0;
	var die2 = 0;
	var diceTotal = 0;
	var winProfit = 4;
	var amountLost = 1;
	var maxWin = wagerValue;
	var rollsMaxAmount = 0;
	var currentRolls = 0;
	var numWins = 0;
	var rollsTable = 0;

	var stats = document.getElementById("stats");

	var die1 = document.getElementById("die1");
	var die2 = document.getElementById("die2");
	var status = document.getElementById("status");
	var d1 = Math.floor(Math.random() * 6) + 1;
	var d2 = Math.floor(Math.random() * 6) + 1;
	var diceTotal = d1 + d2;
		die1.innerHTML = d1;
		die2.innerHTML = d2;
		status.innerHTML ="You rolled "+diceTotal+".";
	if (diceTotal == 7){
		status.innerHTML += "You Win!!";}

	if (wagerValue < amountLost) {
		alert("You need to bet some money to win some money!");
	}
	else if (isNaN(initialWagerValue)) {
		alert("Starting bet needs to be a number!");
	}

	else {
		playGame();
		resultsTable();		
		playAgainButton();
	}

	function playGame(){
	while (wagerValue >= amountLost){
		dice1 = Math.floor(Math.random() * 6) + 1;
		dice2 = Math.floor(Math.random() * 6) + 1;
		diceTotal = dice1 + dice2;

		showEarnings();
		}
	}

	function showEarnings() {
	currentRolls++;
	if (diceTotal == 7) {
		wagerValue += winProfit;
		numWins++;
	} 

	else {
		wagerValue -= amountLost;
	}

	if (wagerValue > maxWin) {
		maxWin = wagerValue;
		rollsMaxAmount = currentRolls;
	}		
		rollsTable[diceTotal]++;
	}
	function resultsTable(){
		document.getElementById("outputWagerResults").innerHTML = "$" + (Number(initialWagerValue)).toFixed(2);	
		document.getElementById("rollsWagerResult").innerHTML = currentRolls;
		document.getElementById("maxAmountResult").innerHTML = "$" + (Number(maxWin)).toFixed(2);
		document.getElementById("rollsMaxAmountResult").innerHTML = rollsMaxAmount;
	}

	function playAgainButton() {
		document.getElementById("playButton").innerHTML = "Play Again?";
		}
	}












